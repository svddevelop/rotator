
//
//

#include <stdio.h>
#include <string.h>
#include <Arduino.h>

// Include the AccelStepper library:
#include <AccelStepper.h>
// Include the Bluetooth Serial library:
#include "BluetoothSerial.h"
#if !defined(CONFIG_BT_ENABLED) || !defined(CONFIG_BLUEDROID_ENABLED)
#error Bluetooth is not enabled! Please run `make menuconfig` to and enable it
#endif

//Create a Bluetooth Serial Object
BluetoothSerial SerialBT;

// Define stepper motor connections and motor interface type. Motor interface type must be set to 1 when using a driver:
#define dirPin              25
#define stepPin             26
#define motorInterfaceType  1
#define MS0                 XX         // Pin XX connected to MS0 pin
#define MS1                 XX         // Pin XX connected to MS1 pin
#define MS2                 XX         // Pin XX connected to MS2 pin
#define home_switch         XXX //


volatile boolean updateFlag = false;//new message received

//Step Sizes
// MS0  MS1   MS2
// LOW  HIGH  LOW   - 1/4 Step
// HIGH HIGH  LOW   - 1/8 Step
// LOW  LOW   HIGH  - 1/16 Step
// HIGH HIGH  HIGH  - 1/32 Step


// Create a new instance of the AccelStepper class:
AccelStepper stepper = AccelStepper(motorInterfaceType, stepPin, dirPin);

volatile uint32_t max_speed = 500, accel = 500, move_to = 0;

void setup() {
  Serial.begin(115200);
  
  SerialBT.begin("Rotator"); //Bluetooth device name
  SerialBT.register_callback(changeSettings);//Arduino IDE

  // Set the maximum speed and acceleration:
  stepper.setMaxSpeed(max_speed);
  stepper.setAcceleration(accel);

}

void process_stepper(){

  Serial.println( F("run"));
  stepper.setMaxSpeed(max_speed);
  stepper.setAcceleration(accel);
  stepper.moveTo(move_to);  

  stepper.runToPosition();
}


void loop()
{

  if (updateFlag)
  {
    updateFlag = false;

    process_stepper();

  }

}

char delimiter[] = " ";

void parse_str(String str){

    // initialize first part (string, delimiter)
    char* pstr = (char*)str.c_str();
    char* ptr = strtok(pstr, delimiter);
    byte i = 0;

    while(ptr != NULL) {

        String sptr = String(ptr);

        if ( i == 0) move_to = sptr.toInt();

        sptr = String(ptr);
        if ( i == 1) max_speed = sptr.toInt();

        sptr = String(ptr);
        if ( i == 2) accel = sptr.toInt();

        // create next part
        ptr = strtok(NULL, delimiter);

        i++;
    }
    Serial.printf_P(PSTR("moveTo:%d speed:%d acceleration:%d\r\n"), move_to, max_speed, accel);
}


void changeSettings(esp_spp_cb_event_t event, esp_spp_cb_param_t *param) 
{
  if (event == ESP_SPP_DATA_IND_EVT) //received data
  {

    String readStr = SerialBT.readString();
    Serial.printf_P(PSTR("reasStr:%s\r\n"), readStr.c_str() );
    if ( ! updateFlag ){ 
      
       parse_str( readStr );
    
       updateFlag = true;
       SerialBT.println( F("YES") );
    }
    else 
       SerialBT.println( F("NO") );
  }
}
